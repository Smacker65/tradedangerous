[ ![Codeship Status for Smacker65/tradedangerous](https://www.codeship.io/projects/3630f2d0-3204-0132-b169-1a0d34a836f9/status)](https://www.codeship.io/projects/40260)

TradeDangerous
==============
Copyright (C) Oliver "kfsone" Smith, July 2014

REQUIRES PYTHON 3.0 OR HIGHER
-----------------------------

![Elite Dangerous](http://img4.wikia.nocookie.net/__cb20140302021500/elite-dangerous/images/thumb/b/bc/EliteDangerousLogo2.png/100px-11,182,0,170-EliteDangerousLogo2.png)

What is Trade Dangerous?
------------------------
TradeDangerous is a cargo run optimizer for Elite: Dangerous that calculates
everything from simple one-jump stops between stations to calculating complex
multi-stop routes with light-year, jumps-per-stop, and all sorts of other
things.

For multi-stop routes, it takes into account the money you are making and
factors that into the shopping for each subsequent hop.
