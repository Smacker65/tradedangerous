#!/usr/bin/env python
# ---------------------------------------------------------------------
# Copyright (C) Oliver 'kfsone' Smith 2014 <oliver@kfs.org>:
#  You are free to use, redistribute, or even print and eat a copy of
#  this software so long as you include this copyright notice.
#  I guarantee there is at least one bug neither of us knew about.
#---------------------------------------------------------------------
# TradeDangerous :: Modules :: Test Module
#
#  Unit tests

######################################################################

import unittest

import trade
import tradedb
import tradecalc


class TesttradeFunctions(unittest.TestCase):
    def setUp(self):
        self.seq = list(range(10))

    def test_sanity(self):
        result = trade.hello()
        self.assertEqual("hello", result)

    # def test_loadDatabase_fromDefaultFile_success(self):
    #     trade.loadDatabase(False, None)
    #     self.assertIsNotNone(trade.tdb)
    #
    # def test_loadDatabase_fromMissingFile_exception(self):
    #     #trade.loadDatabase(False, "IDoNotExist.sql")
    #     self.assertIsNotNone(trade.tdb)


if __name__ == '__main__':
    unittest.main()