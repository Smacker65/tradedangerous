import trade
import sys

args = [
    "-w -v local --ly 11 eranin",
    "--nocaps pillage",
    "run --sh hauler --fr chango --cr 20000",
    "--nocaps nav --ship=type6 5287 2887",
    "-vvv run --sh type6 --cr 50000 --ins 20000 --ly 12 --ju 3 --av anderson,silver --via cuffey --to aulin --fr chango --hops 6",
]
for arg in args:
    sys.argv[0] = "trade.py"
    sys.argv[1:] = arg.split()
    trade.main()